```{eval-rst}
.. currentmodule:: tango
```

(pytango-enhancement-proposals)=

# PyTango Enhancement Proposals

```{toctree}
:maxdepth: 1

tep/tep-0001
tep/tep-0002
```
