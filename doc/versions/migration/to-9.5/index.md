(pytango-migration-guide-9-4-9-5)=

# Moving to v9.5

This chapter describes how to migrate to
PyTango versions 9.5.x from 9.4.x and earlier.

```{toctree}
:maxdepth: 3

deps-install
dev-int
short-name-test-access
```
