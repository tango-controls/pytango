(utilities)=

# The Utilities API

```{eval-rst}
.. currentmodule:: tango.utils
```

```{eval-rst}
.. autoclass:: tango.utils.EventCallback
    :members:
    :undoc-members:
```

```{eval-rst}
.. autofunction:: tango.utils.get_enum_labels
```

```{eval-rst}
.. autofunction:: tango.utils.is_pure_str
```

```{eval-rst}
.. autofunction:: tango.utils.is_seq
```

```{eval-rst}
.. autofunction:: tango.utils.is_non_str_seq
```

```{eval-rst}
.. autofunction:: tango.utils.is_integer
```

```{eval-rst}
.. autofunction:: tango.utils.is_number
```

```{eval-rst}
.. autofunction:: tango.utils.is_bool
```

```{eval-rst}
.. autofunction:: tango.utils.is_scalar_type
```

```{eval-rst}
.. autofunction:: tango.utils.is_array_type
```

```{eval-rst}
.. autofunction:: tango.utils.is_numerical_type
```

```{eval-rst}
.. autofunction:: tango.utils.is_int_type
```

```{eval-rst}
.. autofunction:: tango.utils.is_float_type
```

```{eval-rst}
.. autofunction:: tango.utils.is_bool_type
```

```{eval-rst}
.. autofunction:: tango.utils.is_binary_type
```

```{eval-rst}
.. autofunction:: tango.utils.is_str_type
```

```{eval-rst}
.. autofunction:: tango.utils.obj_2_str
```

```{eval-rst}
.. autofunction:: tango.utils.seqStr_2_obj
```

```{eval-rst}
.. autofunction:: tango.utils.scalar_to_array_type
```

```{eval-rst}
.. autofunction:: tango.utils.get_home
```

```{eval-rst}
.. autofunction:: tango.utils.requires_pytango
```

```{eval-rst}
.. autofunction:: tango.utils.requires_tango
```

```{eval-rst}
.. autofunction:: tango.utils.set_telemetry_tracer_provider_factory
```

```{eval-rst}
.. autofunction:: tango.utils.get_telemetry_tracer_provider_factory
```
