# Logging decorators

```{eval-rst}
.. currentmodule:: tango
```

## LogIt

```{eval-rst}
.. autoclass:: tango.LogIt
    :members:
```

## DebugIt

```{eval-rst}
.. autoclass:: tango.DebugIt
    :members:
```

## InfoIt

```{eval-rst}
.. autoclass:: tango.InfoIt
    :members:
```

## WarnIt

```{eval-rst}
.. autoclass:: tango.WarnIt
    :members:
```

## ErrorIt

```{eval-rst}
.. autoclass:: tango.ErrorIt
    :members:
```

## FatalIt

```{eval-rst}
.. autoclass:: tango.FatalIt
    :members:
```
