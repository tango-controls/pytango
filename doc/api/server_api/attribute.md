# Attribute classes

```{eval-rst}
.. currentmodule:: tango
```

## Attr

```{eval-rst}
.. autoclass:: tango.Attr
    :members:
```

## Attribute

```{eval-rst}
.. autoclass:: tango.Attribute
    :members:
```

## WAttribute

```{eval-rst}
.. autoclass:: tango.WAttribute
    :members:
```

## MultiAttribute

```{eval-rst}
.. autoclass:: tango.MultiAttribute
    :members:
```

## UserDefaultAttrProp

```{eval-rst}
.. autoclass:: tango.UserDefaultAttrProp
    :members:
```
