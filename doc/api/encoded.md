(encoded)=

# Encoded API

*This feature is only possible since PyTango 7.1.4*

```{eval-rst}
.. currentmodule:: tango
```

```{eval-rst}
.. autoclass:: tango.EncodedAttribute
    :members:
```
