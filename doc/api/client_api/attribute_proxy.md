# AttributeProxy

```{eval-rst}
.. currentmodule:: tango
```

```{eval-rst}
.. autoclass:: tango.AttributeProxy
    :members:
```
